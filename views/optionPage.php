<?php

use KrrFaviconMetaTags\Plugin;
use KrrFaviconMetaTags\Settings;
use KrrFaviconMetaTags\Warnings;

$settings = Settings::getInstance();
$plugin   = Plugin::getInstance();
$pageSlug = Plugin::getInstance()->getPageSlug();
?>

<div class="wrap">
	<h1 class="wp-heading-inline"><?= __('Favicon Meta Tags', 'krr-favicon-mt') ?></h1>

	<!-- ********************************************************* *
	 * WARNINGS
	 * *********************************************************** -->
	<?php Warnings::getInstance()->show(); ?>

	<div class="krr-fmt-content">
		<form action="<?= admin_url('themes.php?page=' . $pageSlug) ?>" method="POST" class="krr-fmt-form">
			<h2><?= __('Settings', 'krr-favicon-mt') ?></h2>
			<input type="hidden" name="page" value="<?= $pageSlug ?>">

			<!-- ********************************************************* *
			 * GLOBAL PATH
			 * *********************************************************** -->
			<fieldset id="path">
				<label for="path-field"><?= __('Path to your favicon folder in your active theme', 'krr-favicon-mt') ?></label>
				<input type="text" name="path" id="path-field" value="<?= $settings->getValue('path') ?>">
			</fieldset>

			<fieldset id="color-selection">
				<label for="color-selection-field"><?= __('Selection of the colors', 'krr-favicon-mt') ?></label>
				<select name="color-selection" id="color-selection-field">
					<option value="global" <?= $settings->getValue('color-selection') === 'global' ? 'selected="selected"' : '' ?>">
						<?= __('Globally', 'krr-favicon-mt') ?>
					</option>
					<option value="specific" <?= $settings->getValue('color-selection') === 'specific' ? 'selected="selected"' : '' ?>">
						<?= __('For each application', 'krr-favicon-mt') ?>
					</option>
				</select>
			</fieldset>

			<fieldset id="primary" data-select="global">
				<label for=""><?= __('Primary color', 'krr-favicon-mt') ?></label>
				<input type="text" name="primary-color" class="color-field" value="<?= $settings->getValue('primary-color') ?>">
			</fieldset>

			<fieldset id="secondary" data-select="global">
				<label for=""><?= __('Secondary color', 'krr-favicon-mt') ?></label>
				<input type="text" name="secondary-color" class="color-field" value="<?= $settings->getValue('secondary-color') ?>">
			</fieldset>

			<fieldset id="ios" data-select="specific">
				<label for=""><?= __('iOS - Safari pinned color', 'krr-favicon-mt') ?></label>
				<input type="text" name="ios-color" class="color-field" value="<?= $settings->getValue('ios-color') ?>">
			</fieldset>

			<fieldset id="msapplication-title" data-select="specific">
				<label for=""><?= __('MS Applications - Title color', 'krr-favicon-mt') ?></label>
				<input type="text" name="ms-title-color" class="color-field" value="<?= $settings->getValue('ms-title-color') ?>">
			</fieldset>
			<fieldset id="msapplication-theme" data-select="specific">
				<label for=""><?= __('MS Applications - Theme color', 'krr-favicon-mt') ?></label>
				<input type="text" name="ms-theme-color" class="color-field" value="<?= $settings->getValue('ms-theme-color') ?>">
			</fieldset>
			<fieldset id="android-theme" data-select="specific">
				<label for=""><?= __('Android-Chrome - Theme color', 'krr-favicon-mt') ?></label>
				<input type="text" name="android-theme-color" class="color-field" value="<?= $settings->getValue('android-theme-color') ?>">
			</fieldset>
			<fieldset id="android-bkg" data-select="specific">
				<label for=""><?= __('Android-Chrome - Background color', 'krr-favicon-mt') ?></label>
				<input type="text" name="android-background-color" class="color-field" value="<?= $settings->getValue('android-background-color') ?>">
			</fieldset>

			<!-- ********************************************************* *
			 * SUBMIT BUTTON
			 * *********************************************************** -->
			<div class="krr-fmt-bottom">
				<button type="submit" name="submit-settings" class="button button-primary">
					<span><?= __('Update settings', 'krr-favicon-mt') ?></span>
				</button>
			</div>
		</form>

		<!-- ********************************************************* *
		 * FAVICON EXISTING LIST
		 * *********************************************************** -->
		<div class="krr-fmt-files">
			<h2><?= __('Detected Files', 'krr-favicon-mt') ?></h2>

			<table class="krr-fmt-files-lists wp-list-table widefat striped">
				<thead>
					<tr>
						<?php foreach (array_keys(Plugin::getInstance()->getSizes()) as $device) { ?>
							<th><?= $device ?></th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php foreach (Plugin::getInstance()->getSizes() as $device => $deviceSizes) { ?>
								<td>
									<ul class="krr-fmt-files-sizes">
										<?php foreach ($deviceSizes as $size) {
											$found = $plugin->sizeExist($device, $size);

											if ($size === 'ico') {
												$label = "favicon.ico";
												$fileName = $label;
											} else if ($size === 'mask-icon') {
												$label = "mask-icon.svg";
												$fileName = $device . '-' . $label;
											} else {
												$label = $size . 'px';
												$fileName = $device . '-' . $size . 'x' . $size . '.png';
											}
											?>
											<li title="<?= $fileName ?>">
												<span class="krr-fmt-files-size-status <?= ($found) ? 'good' : 'bad' ?>"></span>
												<span class="krr-fmt-files-size-label"><?= $label ?></span>
											</li>
										<?php } ?>
									</ul>
								</td>
						<?php } ?>
					</tr>
				</tbody>
			</table>

			<div class="krr-fmt-files-lists-note">
				<?= __('The files nomenclature is like this: favicon-32x32.png', 'krr-favicon-mt') ?>
			</div>

			<div class="krr-fmt-recommendation">
				<h3><?= __('We recommend you to add at least the following set of elements:', 'krr-favicon-mt') ?></h3>
				<ul>
					<li><strong>favicon.ico :</strong> <?= __('Original icon format', 'krr-favicon-mt') ?></li>
					<li><strong>favicon-16x16.png </strong> <?= __('Modern equivalent of original ICO format', 'krr-favicon-mt') ?></li>
					<li><strong>favicon-32x32.png </strong> <?= __('Safari', 'krr-favicon-mt') ?></li>
					<li><strong>android-chrome-192x192.png </strong> <?= __('Chrome/Android', 'krr-favicon-mt') ?></li>
					<li><strong>ios-180-180.png :</strong> <?= __('Apple touch icon', 'krr-favicon-mt') ?></li>
					<li><strong>mask-icon.svg :</strong> <?= __('Safari pinned tab SVG', 'krr-favicon-mt') ?></li>
					<li><strong>msapplication-150x150.png :</strong> <?= __('MS tile', 'krr-favicon-mt') ?></li>
				</ul>
			</div>
		</div>

	</div>
</div>
