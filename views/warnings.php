<?php
use \KrrFaviconMetaTags\Warnings;
?>

<div class="krr-fmt-warning">
	<div class="krr-fmt-warning-title">
		<span class="dashicons dashicons-warning"></span>
		<?= __('Warnings', 'krr-favicon-mt') ?>
	</div>
	<ul>
		<?php foreach (Warnings::getInstance()->getErrors() as $error) { ?>
			<li><?= $error ?></li>
		<?php } ?>
	</ul>
</div>