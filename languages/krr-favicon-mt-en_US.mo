��          �       �      �  D   �  !   �          1     @     R  
   [  "   f     �     �  0   �     �  6   �  8   6     o  :   �     �     �  1   �  `     h   p  ;   �          1  �  K  D   �  !   /     Q     n     }     �  
   �  "   �     �     �  2        5  6   >  8   u     �  :   �     	     	  1   	  `   N	  h   �	  ;   
     T
     p
   A plugin that create your favicon meta tags for you for all devices. Android-Chrome - Background color Android-Chrome - Theme color Detected Files Favicon Meta Tags Kalimorr Let's go ! MS Applications - Background color MS Applications - Theme color MS Applications - Title color Path to your favicon folder in your active theme Settings The files nomenclature is like this: favicon-36x36.png The main favicon is missing (format: PNG, sizes: 32px) ! The path does not exist ! The plugin Favicon MetaTags needs to be configured => %1$s Update settings Warnings We have found any favicon in the specified path ! You have at least one icon for Android but one or more colors are missing for this application ! You have at least one icon for MS applications but one or more colors are missing for this application ! You have the mask icon for Safari but no color associated ! https://github.com/Kalimorr iOS - Safari pinned color Project-Id-Version: Favicon MetaTags
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-10 20:22+0000
PO-Revision-Date: 2021-04-06 20:03+0000
Last-Translator: 
Language-Team: English (United States)
Language: en_US
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.2; wp-5.7 A plugin that create your favicon meta tags for you for all devices. Android-Chrome - Background color Android-Chrome - Theme color Detected Files Favicon Meta Tags Kalimorr Let's go ! MS Applications - Background color MS Applications - Theme color MS Applications - Title color Path to your "favicon" folder in your active theme Settings The files nomenclature is like this: favicon-36x36.png The main favicon is missing (format: PNG, sizes: 32px) ! The path does not exist ! The plugin Favicon MetaTags needs to be configured => %1$s Update settings Warnings We have found any favicon in the specified path ! You have at least one icon for Android but one or more colors are missing for this application ! You have at least one icon for MS applications but one or more colors are missing for this application ! You have the mask icon for Safari but no color associated ! https://github.com/Kalimorr iOS - Safari pinned color 