#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Favicon MetaTags\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-06 20:32+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.3.3; wp-5.4.1"

#. Description of the plugin
msgid "A plugin that create your favicon meta tags for you for all devices."
msgstr ""

#: views/optionPage.php:73
msgid "Android-Chrome - Background color"
msgstr ""

#: views/optionPage.php:69
msgid "Android-Chrome - Theme color"
msgstr ""

#: views/optionPage.php:143
msgid "Apple touch icon"
msgstr ""

#: views/optionPage.php:142
msgid "Chrome/Android"
msgstr ""

#: views/optionPage.php:91
msgid "Detected Files"
msgstr ""

#. Name of the plugin
#: classes/Plugin.php:181 classes/Plugin.php:182 views/optionPage.php:13
msgid "Favicon Meta Tags"
msgstr ""

#: views/optionPage.php:40
msgid "For each application"
msgstr ""

#: views/optionPage.php:37
msgid "Globally"
msgstr ""

#. Author URI of the plugin
msgid "https://github.com/kofinorr"
msgstr ""

#: views/optionPage.php:56
msgid "iOS - Safari pinned color"
msgstr ""

#. Author of the plugin
msgid "Kofinorr"
msgstr ""

#: classes/Plugin.php:264
msgid "Let's go !"
msgstr ""

#: views/optionPage.php:140
msgid "Modern equivalent of original ICO format"
msgstr ""

#: views/optionPage.php:65
msgid "MS Applications - Theme color"
msgstr ""

#: views/optionPage.php:61
msgid "MS Applications - Title color"
msgstr ""

#: views/optionPage.php:145
msgid "MS tile"
msgstr ""

#: views/optionPage.php:139
msgid "Original icon format"
msgstr ""

#: views/optionPage.php:29
msgid "Path to your favicon folder in your active theme"
msgstr ""

#: views/optionPage.php:46
msgid "Primary color"
msgstr ""

#: views/optionPage.php:141
msgid "Safari"
msgstr ""

#: views/optionPage.php:144
msgid "Safari pinned tab SVG"
msgstr ""

#: views/optionPage.php:51
msgid "Secondary color"
msgstr ""

#: views/optionPage.php:34
msgid "Selection of the colors"
msgstr ""

#: views/optionPage.php:22
msgid "Settings"
msgstr ""

#: views/optionPage.php:133
msgid "The files nomenclature is like this: favicon-32x32.png"
msgstr ""

#: classes/Warnings.php:88
msgid "The main favicon is missing (format: PNG, sizes: 32px) !"
msgstr ""

#: classes/Warnings.php:78
msgid "The path does not exist !"
msgstr ""

#: classes/Plugin.php:262
#, php-format
msgid "The plugin Favicon MetaTags needs to be configured => %1$s"
msgstr ""

#: views/optionPage.php:82
msgid "Update settings"
msgstr ""

#: views/warnings.php:8
msgid "Warnings"
msgstr ""

#: classes/Warnings.php:83
msgid "We have found any favicon in the specified path !"
msgstr ""

#: views/optionPage.php:137
msgid "We recommend you to add at least the following set of elements:"
msgstr ""

#: classes/Warnings.php:102
msgid ""
"You have at least one icon for Android but one or more colors are missing "
"for this application !"
msgstr ""

#: classes/Warnings.php:95
msgid ""
"You have at least one icon for MS applications but one or more colors are "
"missing for this application !"
msgstr ""

#: classes/Warnings.php:107
msgid "You have the mask icon for Safari but no color associated !"
msgstr ""
