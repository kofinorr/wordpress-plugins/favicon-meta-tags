# Favicon Meta Tags

A plugin that create your favicon meta tags for you for all devices.

## Requirements
* Require WordPress 4.7+ / Tested up to 5.7
* Require PHP 5.6



## Installation

### Manual
- Download and install the plugin using the built-in WordPress plugin installer.
- No settings necessary, it's a Plug-and-Play plugin !

### Composer
- Add the following repository source : 
```json
{
    "type": "vcs",
    "url": "https://gitlab.com/kofinorr/wordpress-plugins/favicon-meta-tags.git"
}
```
- Include the following line into your composer file for last master's commits or a tag released.
```json
"kofinorr/favicon-meta-tags": "dev-master"
```

- No settings necessary, it's a Plug-and-Play plugin !

## How To Use

- Create a folder named `favicon` wherever you want in your theme folder ;

- Place all your favicon files into this new folder with this nomenclature : **{category}-{size}-{size}.png** ;
- Go to `Appearance > Favicon Meta Tags` to access the configuration panel ;
- Specify the path from your theme to access the `favicon` folder you have created earlier ;
- Choose the color of the different applications associated to your favicon ;
- You can hover each item in the table to know the file name nomenclature.

On the right side you have a dashboard that indicate the presence of each size of the favicon. Notice that you does not have to have all of them.

If something goes wrong, you will have a notice box with all the recommendations. 

## Internationalisation
* English *(default)*
* Français

# License
"Favicon Meta Tags" is licensed under the GPLv3 or later.

