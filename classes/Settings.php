<?php

namespace KrrFaviconMetaTags;

/**
 * Class Settings
 * @package KrrFaviconMetaTags
 */
class Settings
{
	/** @var string */
	public $optionName = 'favicon-metatags';

	/** @var string */
	private $options = [];

	/**
	 * @var Settings|null  Instance of the current class
	 */
	public static $instance = null;

	/**
	 * Plugin constructor.
	 */
	public function __construct()
	{
		add_action('admin_notices', [$this, 'saveNotices']);
	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Settings|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Settings();
		}

		return self::$instance;
	}

	/**
	 * @return string
	 */
	public function getOptionName(): string
	{
		return $this->optionName;
	}

	/**
	 * Save settings options in database
	 */
	public function saveOptions ()
	{
		/* Security if the form has not been submitted */
		if (!isset($_POST['submit-settings'])) {
			return;
		}

		/* Start with an empty array to fill it with the new settings */
		$this->options = [];

		if (isset($_POST['submit-settings'])) {
			$this->saveValue('path');
			$this->saveValue('color-selection');
			$this->saveValue('primary-color');
			$this->saveValue('secondary-color');
			$this->saveValue('ios-color');
			$this->saveValue('ms-title-color');
			$this->saveValue('ms-theme-color');
			$this->saveValue('ms-background-color');
			$this->saveValue('android-theme-color');
			$this->saveValue('android-background-color');
		}

		/* Update the settings in DB */
		update_option($this->getOptionName(), $this->options);

		/* After saving, reload the config files */
		Generator::getInstance()->xmlConfig();
		Generator::getInstance()->manifestConfig();
		//Warnings::getInstance()->setErrors();
	}

	/**
	 * Save the value of a field if it has been filled
	 * @param string $field
	 */
	private function saveValue ($field)
	{
		if (isset($_POST[$field])) {
			$this->options[$field] = $_POST[$field];
		}
	}

	/**
	 * Get the value of a field
	 *
	 * @param $fieldKey
	 *
	 * @return string
	 */
	public function getValue($fieldKey)
	{
		$option = get_option($this->getOptionName());

		if (is_array($option) && key_exists($fieldKey, $option)) {
			return $option[$fieldKey];
		} else {
			return null;
		}
	}

	/**
	 * Check if the folder is empty
	 * @param $dir
	 * @return bool
	 */
	public function checkEmptyFolder($dir)
	{
		if (!file_exists($dir)) {
			return true;
		}

		$handle = opendir($dir);

		if (is_bool($handle)) {
			return true;
		}

		while (false !== ($entry = readdir($handle))) {
			if ($entry != "." && $entry != "..") {
				closedir($handle);
				return false;
			}
		}
		closedir($handle);
		return true;
	}

	/**
	 * Notice when the options are saved
	 */
	public function saveNotices()
	{
		if (isset($_POST['submit-settings'])) { ?>
			<div class="notice notice-success">
				<p><strong><?= __('Changes saved.', 'default') ?></strong></p>
			</div>
		<?php }
	}
}