<?php
/**
 * Plugin Name: Favicon Meta Tags
 * Description: A plugin that create your favicon meta tags for you for all devices.
 * Version: 1.0.1
 * Author: Kofinorr
 * Author URI: https://kofinorr.damien-roche.fr/
 * Text Domain: krr-favicon-mt
 * Domain Path: /languages
 * License: GPLv2 or later
 */

use \KrrFaviconMetaTags\Plugin;

/* Security */
defined('ABSPATH')
or die ('no');

define('KRR_FAVICON_MT_FILE', __FILE__);

/* Call the files */
require_once 'classes/Install.php';
require_once 'classes/Plugin.php';
require_once 'classes/Generator.php';
require_once 'classes/Settings.php';
require_once 'classes/Warnings.php';

add_action('plugins_loaded', function() {
	Plugin::getInstance();
}, 1);

/* Activation and deactivation system */
register_activation_hook( KRR_FAVICON_MT_FILE, ['\KrrFaviconMetaTags\Install', 'activation'] );
register_deactivation_hook( KRR_FAVICON_MT_FILE, ['\KrrFaviconMetaTags\Install', 'deactivation'] );
